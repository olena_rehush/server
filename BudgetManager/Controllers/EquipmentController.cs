﻿using System.Collections.Generic;
using System.Linq;
using DataAccess.Models;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EquipmentController : ControllerBase
    {
        private readonly BudgetContext _context;
        public EquipmentController(BudgetContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Equipment>> GetAllEquipments()
        {
            return _context.Equipments.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Equipment> GetEquipment(int id)
        {
            var item = _context.Equipments.FirstOrDefault(c => c.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public IActionResult CreateEquipment(Equipment item)
        {
            _context.Equipments.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetEquipment", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateEquipment(int id, Equipment item)
        {
            var equipment = _context.Equipments.FirstOrDefault(c => c.Id == id);
            if (equipment == null)
            {
                return NotFound();
            }

            equipment = item;
            equipment.Id = id;

            _context.Equipments.Update(equipment);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteEquipment(int id)
        {
            var equipment = _context.Equipments.FirstOrDefault(c => c.Id == id);
            if (equipment == null)
            {
                return NotFound();
            }

            _context.Equipments.Remove(equipment);
            _context.SaveChanges();
            return NoContent();
        }
}
}