﻿using System.Collections.Generic;
using System.Linq;
using DataAccess.Models;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly BudgetContext _context;
        public EmployeeController(BudgetContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Employee>> GetAllEmployees()
        {
            return _context.Employees.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<Employee> GetEmployee(int id)
        {
            var item = _context.Employees.FirstOrDefault(c => c.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public IActionResult CreateEmployee(Employee item)
        {
            _context.Employees.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetEmployee", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateEmployee(int id, Employee item)
        {
            var employee = _context.Employees.FirstOrDefault(c => c.Id == id);
            if (employee == null)
            {
                return NotFound();
            }

            employee = item;
            employee.Id = id;

            _context.Employees.Update(employee);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteEmployee(int id)
        {
            var employee = _context.Employees.FirstOrDefault(c => c.Id == id);
            if (employee == null)
            {
                return NotFound();
            }

            _context.Employees.Remove(employee);
            _context.SaveChanges();
            return NoContent();
        }
    }
}