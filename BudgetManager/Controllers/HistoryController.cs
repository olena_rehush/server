﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Models;
using DataAccess.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HistoryController : ControllerBase
    {
        private readonly BudgetContext _context;
        public HistoryController(BudgetContext context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult<IEnumerable<History>> GetAllHistory()
        {
            return _context.Histories.ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<History> GetHistory(int id)
        {
            var item = _context.Histories.FirstOrDefault(c => c.Id == id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }

        [HttpPost]
        public IActionResult CreateHistory(History item)
        {
            _context.Histories.Add(item);
            _context.SaveChanges();

            return CreatedAtRoute("GetHistory", new { id = item.Id }, item);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateHistory(int id, History item)
        {
            var history = _context.Histories.FirstOrDefault(c => c.Id == id);
            if (history == null)
            {
                return NotFound();
            }

            history = item;
            history.Id = id;

            _context.Histories.Update(history);
            _context.SaveChanges();
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteHistory(int id)
        {
            var history = _context.Histories.FirstOrDefault(c => c.Id == id);
            if (history == null)
            {
                return NotFound();
            }

            _context.Histories.Remove(history);
            _context.SaveChanges();
            return NoContent();
        }
    }
}