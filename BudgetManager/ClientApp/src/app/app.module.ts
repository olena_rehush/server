import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CreateComponent } from './components/create/create.component';
import { EditComponent } from './components/edit/edit.component';
import { IndexComponent } from './components/index/index.component';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatSidenavModule,
  MatFormFieldModule,
  MatInputModule,
  MatTooltipModule,
  MatListModule,
  MatSortModule,
  MatTableModule,
  MatSelectModule,
  MatOptionModule,
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatTabsModule,
  MatGridListModule,
} from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { MainService } from './main.service';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AppService } from './services/app.service';

const routes: Routes = [
  {
    path: 'index',
    component: IndexComponent
  }
]

const materialModules = [
  BrowserAnimationsModule,
  MatButtonModule,
  MatSelectModule,
  MatOptionModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatListModule,
  MatCardModule,
  MatSidenavModule,
  MatFormFieldModule,
  MatInputModule,
  MatTooltipModule,
  MatTableModule,
  MatSortModule,
  MatDialogModule,
  MatGridListModule
];

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    materialModules,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    materialModules,

  ],
  providers: [
    MainService,
    AppService,
  ],
  entryComponents:[],
  bootstrap: [AppComponent]
})
export class AppModule { }
