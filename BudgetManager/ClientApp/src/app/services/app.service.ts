import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  uri: String = 'https://localhost:44376/api'

  constructor(private http: HttpClient) { }

  get(controller){
    return this
          .http
          .get(`${this.uri}/${controller}`);
  }

  getById(controller, id){
    return this
          .http
          .get(`${this.uri}/${controller}/${id}`);
  }

  create(controller, item){
    this
      .http
      .post(`${this.uri}/${controller}`, item)
      .subscribe(res=>
        alert(res)
      );
  }

  update(controller, id, item){
    this
      .http
      .put(`${this.uri}/${controller}/${id}`, item)
      .subscribe(res=>
        alert(res)
      );
  }

  delete(controller, id){
    return this
          .http
          .delete(`${this.uri}/${controller}/${id}`);
  }
}

