﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Models
{
    public class History
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public string Text { get; set; }
        public string Author { get; set; }
        public string FirstName { get; set; }
        public Equipment Equipment { get; set; }
    }
}
