﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Models
{
    public class Equipment
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime AriveDate { get; set; }
        public string Characteristic1 { get; set; }
        public string WorkState { get; set; }
        public History History { get; set; }
    }
}
